"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var manItem = (function () {
    function manItem(surname, rate) {
        this.surname = surname;
        this.rate = rate;
    }
    return manItem;
}());
exports.manItem = manItem;
var AppComponent = (function () {
    function AppComponent() {
        this.items = [
            { surname: lastLetterUpperCase("пупкин"), rate: 5 },
            { surname: lastLetterUpperCase("очевидн0сть5"), rate: 3 }
        ];
    }
    AppComponent.prototype.manItemAdd = function (surname, rate) {
        if (surname == null || surname == undefined || surname.trim() == "")
            return;
        surname = lastLetterUpperCase(surname);
        //можно было использовать в number onkeypress="return false", но так интереснее..
        if ((rate != null) && (rate != undefined) && (typeof rate === "number") && (rate <= 10) && (rate >= 0) && (~~rate == rate)) {
            this.items.push(new manItem(surname, rate));
        }
        else {
            alert("Оценка должна быть записана одной десятичной цифрой!");
        }
    };
    AppComponent.prototype.manItemDelete = function (deleteManIndex) {
        this.items.splice(deleteManIndex, 1);
    };
    AppComponent.prototype.manItemSort = function () {
        this.items.sort(function (a, b) {
            if (a.surname > b.surname) {
                return 1;
            }
            if (a.surname < b.surname) {
                return -1;
            }
            return 0;
        });
    };
    AppComponent.prototype.manItemRandom = function () {
        var SurnamePartOne = ["иван", "сидор", "звер", "слон", "еж", "григор", "пуп"];
        var SurnamePartTwo = ["ьев", "ов", "идзе", "ко", "ко", "ян", "кин"];
        this.items.push(new manItem(lastLetterUpperCase(SurnamePartOne[getRandomInt(0, 6)] + SurnamePartTwo[getRandomInt(0, 6)]), [getRandomInt(0, 9)]));
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: "\n    <div class=\"panel\">\n        <h4>\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C</h4>\n\t<div class=\"form-strap\">\n\t    <div class=\"row\">\n\t\t<div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-6\">\n\t\t\t<input class=\"form-control\" [(ngModel)]=\"surname\" placeholder = \"\u0444\u0430\u043C\u0438\u043B\u0438\u042F\" />\n\t\t</div>\n\t\t<div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-6\">\n\t\t\t<input type=\"number\" min=\"0\" max=\"9\" class=\"form-control\" [(ngModel)]=\"rate\" placeholder=\"\u041E\u0446\u0435\u043D\u043A\u0430\" />\n\t\t</div>\n\t\t<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\n\t\t    <button class=\"btn btn-success btn-block\" (click)=\"manItemAdd(surname, rate)\">\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C</button>\n\t\t</div>\n\t    </div>\n        </div>\n\t<h4>\u0414\u043E\u043F\u043E\u043B\u043D\u0438\u0442\u0435\u043B\u044C\u043D\u044B\u0435 \u0444\u0443\u043D\u043A\u0446\u0438\u0438</h4>\n\t<div class=\"form-strap\">\n\t    <div class=\"row\">\n\t\t<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\n\t\t    <button class=\"btn btn-success btn-block\" (click)=\"manItemSort()\">\u0421\u043E\u0440\u0442\u0438\u0440\u043E\u0432\u043A\u0430 \u043F\u043E \u0430\u043B\u0444\u0430\u0432\u0438\u0442\u0443</button>\n\t\t</div>\n\t\t<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\n\t\t    <button class=\"btn btn-success btn-block\" (click)=\"manItemRandom()\">\u0421\u043B\u0443\u0447\u0430\u0439\u043D\u044B\u0439 \u043F\u0435\u0440\u0441\u043E\u043D\u0430\u0436</button>\n\t\t</div>\n\t    </div>\n        </div>\n\t<h4>\u041D\u0435\u043F\u0440\u0430\u0432\u0438\u043B\u044C\u043D\u044B\u0439 \u0441\u043F\u0438\u0441\u043E\u043A</h4>\n\t<div class=\"row\">\n\t    <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">    \n\t\t<table class=\"table table-condensed\">\n\t\t    <tbody>\n\t\t\t<tr *ngFor=\"let item of items; let itemIndex = index\">\n\t\t\t    <td width=\"15%\">{{itemIndex+1}}</td>\n\t\t\t    <td width=\"50%\">{{item.surname}}</td>\n\t\t\t    <td width=\"15%\">{{item.rate}}</td>\n\t\t\t    <td width=\"20%\">\n\t\t\t\t<button class=\"btn btn-danger btn-block \" (click)=\"manItemDelete(itemIndex)\">\n\t\t\t\t    <span class=\"glyphicon glyphicon-trash\"></span>\n\t\t\t\t</button>\n\t\t\t    </td>\n\t\t\t</tr>\n\t\t    </tbody>    \n\t\t</table>\n\t    </div>\n\t</div>\t\n    </div>"
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
function lastLetterUpperCase(surname) {
    for (var itemChar = surname.length - 1; itemChar >= 0; itemChar--) {
        if (/[A-Z,a-z,А-Я,а-я]/i.test(surname.charAt(itemChar)))
            return surname.substring(0, itemChar) + surname[itemChar].toUpperCase() + surname.substring(itemChar + 1);
    }
    ;
    return surname;
}
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 0.5)) + min;
}
;
//# sourceMappingURL=app.component.js.map