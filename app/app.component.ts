import { Component } from '@angular/core';
     
export class manItem{
    surname: string;
    rate: number;
     
    constructor(surname: string, rate: number) {
  
        this.surname = surname;
        this.rate = rate;
    }
}
 
@Component({
    selector: 'my-app',
    template: `
    <div class="panel">
        <h4>Добавить</h4>
	<div class="form-strap">
	    <div class="row">
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
			<input class="form-control" [(ngModel)]="surname" placeholder = "фамилиЯ" />
		</div>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
			<input type="number" min="0" max="9" class="form-control" [(ngModel)]="rate" placeholder="Оценка" />
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		    <button class="btn btn-success btn-block" (click)="manItemAdd(surname, rate)">Добавить</button>
		</div>
	    </div>
        </div>
	<h4>Дополнительные функции</h4>
	<div class="form-strap">
	    <div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		    <button class="btn btn-success btn-block" (click)="manItemSort()">Сортировка по алфавиту</button>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		    <button class="btn btn-success btn-block" (click)="manItemRandom()">Случайный персонаж</button>
		</div>
	    </div>
        </div>
	<h4>Неправильный список</h4>
	<div class="row">
	    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">    
		<table class="table table-condensed">
		    <tbody>
			<tr *ngFor="let item of items; let itemIndex = index">
			    <td width="15%">{{itemIndex+1}}</td>
			    <td width="50%">{{item.surname}}</td>
			    <td width="15%">{{item.rate}}</td>
			    <td width="20%">
				<button class="btn btn-danger btn-block " (click)="manItemDelete(itemIndex)">
				    <span class="glyphicon glyphicon-trash"></span>
				</button>
			    </td>
			</tr>
		    </tbody>    
		</table>
	    </div>
	</div>	
    </div>`
})
export class AppComponent { 
    items: manItem[] = 
    [
        { surname: lastLetterUpperCase("пупкин"), rate: 5 },
        { surname: lastLetterUpperCase("очевидн0сть5"), rate: 3 }
    ];
    
    manItemAdd(surname: string, rate: number): void {     
        if(surname==null || surname==undefined || surname.trim()=="")
            return;
	surname=lastLetterUpperCase(surname);
	
	//можно было использовать в number onkeypress="return false", но так интереснее..
	if((rate!=null)&&(rate!=undefined)&&(typeof rate==="number")&&(rate<=10)&&(rate>=0)&&(~~rate==rate)){
	    this.items.push(new manItem(surname, rate));
	}else{
	    alert("Оценка должна быть записана одной десятичной цифрой!")
	}
    }
    
    manItemDelete(deleteManIndex: any): void { 
        this.items.splice(deleteManIndex, 1);
    }
    
    manItemSort(): void { 
        this.items.sort(function (a, b) {
	    if (a.surname > b.surname) {
	        return 1;
	    }
	    if (a.surname < b.surname) {
	        return -1;
	    }
	    return 0;
	});
    }
    
        manItemRandom(): void { 
        let SurnamePartOne = ["иван","сидор","звер","слон","еж","григор","пуп"];
	let SurnamePartTwo = ["ьев","ов","идзе","ко","ко","ян","кин"];
	this.items.push(new manItem(lastLetterUpperCase(SurnamePartOne[getRandomInt(0,6)]+SurnamePartTwo[getRandomInt(0,6)]), [getRandomInt(0,9)]));
    }
}

function lastLetterUpperCase(surname: string)
{
    for(let itemChar=surname.length-1;itemChar>=0;itemChar--){
	 if(/[A-Z,a-z,А-Я,а-я]/i.test(surname.charAt(itemChar)))
	    return surname.substring(0,itemChar)+surname[itemChar].toUpperCase()+surname.substring(itemChar+1);
    };
    return surname; 
}

function getRandomInt(min, max) {
 return Math.floor(Math.random() * (max - min+0.5)) + min;
};